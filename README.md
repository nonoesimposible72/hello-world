# Hello World

An example project I created to learn about git and Gitlab.

# How to run

Type

```
python hello_world.py
```

Both Python 2 and Python 3 are supported.
